﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateGameVer : MonoBehaviour
{

    public Text Text;
    // Start is called before the first frame update
    void Start()
    {
        Text.text = Application.version;
    }

    
}
