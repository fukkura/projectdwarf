// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PunPlayerScores.cs" company="Exit Games GmbH">
//   Part of: Photon Unity Utilities,
// </copyright>
// <summary>
//  Scoring system for PhotonPlayer
// </summary>
// <author>developer@exitgames.com</author>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Photon.Pun;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace Photon.Pun.UtilityScripts
{
    /// <summary>
    /// Scoring system for PhotonPlayer
    /// </summary>
    public class PunPlayerScores : MonoBehaviour
    {
        public const string PlayerScoreProp = "score";
        public const string PlayerGoldProp = "gold";
        public const string PlayerHealthProp = "health";
    }

    public static class ScoreExtensions
    {
        public static void SetScore(this Player player, int newScore)
        {
            Hashtable score = new Hashtable();  // using PUN's implementation of Hashtable
            score[PunPlayerScores.PlayerScoreProp] = newScore;

            player.SetCustomProperties(score);  // this locally sets the score and will sync it in-game asap.
        }

        public static void AddScore(this Player player, int scoreToAddToCurrent)
        {
            int current = player.GetScore();
            current = current + scoreToAddToCurrent;

            Hashtable score = new Hashtable();  // using PUN's implementation of Hashtable
            score[PunPlayerScores.PlayerScoreProp] = current;

            player.SetCustomProperties(score);  // this locally sets the score and will sync it in-game asap.
        }

        public static int GetScore(this Player player)
        {
            object score;
            if (player.CustomProperties.TryGetValue(PunPlayerScores.PlayerScoreProp, out score))
            {
                return (int)score;
            }

            return 0;
        }

        #region Gold
        public static void SetGold(this Player player, int newGold)
        {
            Hashtable gold = new Hashtable();  // using PUN's implementation of Hashtable
            gold[PunPlayerScores.PlayerGoldProp] = newGold;

            player.SetCustomProperties(gold);  // this locally sets the score and will sync it in-game asap.
        }

        public static void AddGold(this Player player, int scoreToAddToCurrent)
        {
            int current = player.GetGold();
            current = current + scoreToAddToCurrent;

            //update UI here?
            

            Hashtable gold = new Hashtable();  // using PUN's implementation of Hashtable
            gold[PunPlayerScores.PlayerGoldProp] = current;

            player.SetCustomProperties(gold);  // this locally sets the score and will sync it in-game asap.
        }

        public static int GetGold(this Player player)
        {
            object gold;
            if (player.CustomProperties.TryGetValue(PunPlayerScores.PlayerGoldProp, out gold))
            {
                return (int)gold;
            }

            return 0;
        }

        public static void ClearGold(this Player player)
        {
            Hashtable gold = new Hashtable();  // using PUN's implementation of Hashtable
            gold[PunPlayerScores.PlayerGoldProp] = 0;

            player.SetCustomProperties(gold);  // this locally sets the score and will sync it in-game asap.
        }

        #endregion


        #region Health

        public static void SetHealth(this Player player, int newHealth)
        {
            Hashtable score = new Hashtable();  // using PUN's implementation of Hashtable
            score[PunPlayerScores.PlayerHealthProp] = newHealth;

            player.SetCustomProperties(score);  // this locally sets the score and will sync it in-game asap.
        }

        public static void AddHealth(this Player player, int healthToAddToCurrent)
        {
            int current = player.GetHealth();
            current = current + healthToAddToCurrent;

            Hashtable score = new Hashtable();  // using PUN's implementation of Hashtable
            score[PunPlayerScores.PlayerHealthProp] = current;

            player.SetCustomProperties(score);  // this locally sets the score and will sync it in-game asap.
        }

        public static int GetHealth(this Player player)
        {
            object health;
            if (player.CustomProperties.TryGetValue(PunPlayerScores.PlayerHealthProp, out health))
            {
                return (int)health;
            }

            return 0;
        }

        #endregion
    }
}