﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviourPunCallbacks
{
    private NetworkConnection _network;
    private void Start()
    {
        _network = FindObjectOfType<NetworkConnection>();
    }

    public void ReturnToMainMenu()
    {
        PhotonNetwork.LeaveRoom();

    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();

        //might need to unload the player badges...

        SceneManager.LoadScene(0);
    }

}
