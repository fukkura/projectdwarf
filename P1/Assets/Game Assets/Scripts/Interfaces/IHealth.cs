﻿using System.Collections;
using System.Collections.Generic;

public interface IHealth 
{

    void TakeDamage(int amount);

    void GiveHealth(int amount);

    bool IsDead();
}
