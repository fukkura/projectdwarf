﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPickup
{
    void Pickup(Player player, GameObject gameObject = null);

}
