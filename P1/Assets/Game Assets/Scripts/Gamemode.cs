﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemode : MonoBehaviour
{
    public int MatchLength;

    public bool StartTimer;



    private double _timerIncrementValue;
    public virtual void OnGameStart()
    {
        //anything that will need doing on the match start will go here...


    }

    public virtual void OnGameUpdate()
    {
        //logic for spawning and that goes here, match length will be controlled by match manager, the length will be gotten from this class





        //check for match end contidtions here



    }

    public virtual void OnGameEnd()
    {
        //what happens for the match to end

    }
}
