﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NetworkConnection : MonoBehaviourPunCallbacks
{
    //need general connection
    //this will be taking over the connection part of MatchManager.cs




    [Header("UI")]
    public InputField PlayerName;
    public InputField RoomCode;
    [Space]
    public GameObject ConnectionPanel;

    private bool _isConnected;
    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        PhotonNetwork.GameVersion = Application.version;

        SceneManager.sceneLoaded += OnLevelLoaded;
    }


    public void ConnectToOnline()
    {
        _isConnected = PhotonNetwork.IsConnectedAndReady;
        if (!PhotonNetwork.IsConnectedAndReady)
        {
            _isConnected = PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        if (_isConnected)
        {
            //enable transition scene and connection Ui object
            ConnectionPanel.SetActive(true);
        }
        else
            Debug.LogError("Unable to connect to Photon Servers");
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        //we can create the room from here...

        //here we will show the player connect UI
        //ConnectionPanel.SetActive(true);

    }


    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();


        //load in the new scene here...

        PhotonNetwork.LoadLevel(1);

    }

    public void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 4;
        roomOptions.CleanupCacheOnLeave = true;
        PhotonNetwork.JoinOrCreateRoom(RoomCode.text, roomOptions, TypedLobby.Default);

        PhotonNetwork.NickName = PlayerName.text;
    }

    public void FindMatch()
    {
        PhotonNetwork.JoinRandomRoom();
        PhotonNetwork.NickName = PlayerName.text;

    }

    public void DisconnectFromServer()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.Disconnect();
        }
    }

    //join a room
    //then move to that scene

    #region Scene Management
    bool doubleCheck = false;
    private void OnLevelLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Room was joined");
        if (scene.buildIndex == 0)
        {
            SceneManager.sceneLoaded -= OnLevelLoaded;
            Destroy(this.gameObject);
            return;
        }

        if (scene.buildIndex != 0 && PhotonNetwork.InRoom ) //need another check to see if the player has a character already spawned
        {
            doubleCheck = true;
            FindObjectOfType<MatchManager>().OnJoinedMatchRoom();

        }

    }


    #endregion
}
