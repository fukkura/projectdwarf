﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchSpawnManager : MonoBehaviourPun
{

    //convert to singleton pattern later on..



    public GameObject ChestPrefab;
    public GameObject GoldPrefab;
    public GameObject ProjectilePrefab;


    [Header("Spawn points")]
    public Transform[] ChestSpawnPoints;

    private int _activeChests;
    private int _activePickups;

    public void SpawnStart()
    {
        StartCoroutine(SpawnRandomChests());
        StartCoroutine(SpawnRandomHealth());
    }
    public void SpawnEnd()
    {
        StopCoroutine("SpawnRandomChests");
        StopCoroutine("SpawnRandomHealth");
    }

    public IEnumerator SpawnRandomChests()
    {
        while (true)
        {

            if (PhotonNetwork.IsMasterClient)
            {

                Chest[] chests = FindObjectsOfType<Chest>(); //this is costly
                _activeChests = chests.Length;


                //need to add a minumn time for this. so might be 

                if (_activeChests < 1)
                {
                    SpawnChest();
                }
            }
            //so if we have less than a certain amount of chests, we should spawn one?

            yield return new WaitForSeconds(Random.Range(2, 7));// this is a testing time' will have random number between 2-10;
        }
    }


    public IEnumerator SpawnRandomHealth()
    {
        while (true)
        {

            if (PhotonNetwork.IsMasterClient)
            {

                Health[] healths = FindObjectsOfType<Health>(); //this is costly
                _activePickups = healths.Length;


                //need to add a minumn time for this. so might be 

                if (_activePickups < 2)
                {
                    SpawnHealthPickup();
                }
            }
            //so if we have less than a certain amount of chests, we should spawn one?

            yield return new WaitForSeconds(Random.Range(4, 12));// this is a testing time' will have random number between 2-10;
        }
    }

    public void SpawnChest()
    {
        SpawnItemByName(ChestPrefab.name);
    }

    public void SpawnGold(Vector2 pos, int amount = 10)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject go = PhotonNetwork.Instantiate(GoldPrefab.name, pos, Quaternion.identity); //spawn the object on the network


            float x = Random.Range(-1f, 1f);
            float y = Random.Range(-1f, 1f);
            Vector2 direction = new Vector2(x, y); //give it a nradom fly off direction
            //if you need the vector to have a specific length:
            direction = direction.normalized * 2f; //normalise it 

            go.GetComponent<Rigidbody2D>().AddForce((direction + Vector2.up) * 2f, ForceMode2D.Impulse); //apply the force to the rigidbody
        }

    }

    public void SpawnProjectile(Vector2 pos, Vector2 dir, float force = 1)
    {
        //just move teh logic from the player combat, so we have a central store of all the projectiles in here..




        GameObject go = PhotonNetwork.Instantiate(ProjectilePrefab.name, pos, Quaternion.identity); // this might be moved to a object pooler system

        BasicProjectile pro = go.GetComponent<BasicProjectile>();
        pro.enabled = true;
        pro.Init(dir, force);

    }

    private void SpawnItemByName(string prefabName)
    {
        int random = Random.Range(0, ChestSpawnPoints.Length);


        //spawn it at one of the random spots, if there isn't a chest currently in range of that point..

        //we should spawn it the furthest point away from the player



        PhotonNetwork.Instantiate(prefabName, ChestSpawnPoints[random].position, Quaternion.identity); // erm this should just work?


    }


    public void SpawnHealthPickup()
    {
        SpawnItemByName("Health Pickup");
    }
    public void SpawnGhost(Vector2 pos)
    {
        PhotonNetwork.Instantiate("Ghost", pos, Quaternion.identity); //spawn the object on the network
    }
}
