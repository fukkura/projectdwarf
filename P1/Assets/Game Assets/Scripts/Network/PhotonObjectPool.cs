﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PhotonObjectPool : MonoBehaviourPun, IPunPrefabPool
{
    public ObjectPool ObjectPool;

    private void Start()
    {
        PhotonNetwork.PrefabPool = this;
    }

    public void Destroy(GameObject gameObject)
    {
        ObjectPool.Despawn(gameObject);

    }

    public GameObject Instantiate(string prefabId, Vector3 position, Quaternion rotation)
    {
        GameObject go = ObjectPool.Spawn(prefabId);

        go.transform.position = position;
        go.transform.rotation = rotation;

        return go;
    }



}
