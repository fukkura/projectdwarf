﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using Photon.Pun.UtilityScripts;

public class MatchManager : MonoBehaviourPunCallbacks
{

    public GameObject PlayerPrefab;
    public Transform[] SpawnPoints;


    [Header("UI")]
    public Button StartButton;
    public GameObject LobbyFakePanel;
    [Space]
    public GameObject WinPanel;
    public Button PlayAgainButton;
    public Text WinnerName;
    public Text WinAmount;
    [Space]
    public GameObject GamePanel;
    public Text GameTimeText;

    public Text PlayerHealthText;
    public Text GoldText;

    [Space]
    public GameObject ConnectionPanel;
    public InputField PlayerName;
    public InputField RoomCode;

    [Space]
    public Text[] PlayerNames;

    [Header("Match")]
    public MatchSpawnManager SpawnManager;
    public int RequiredPlayers;
    public PlayerBannerManager PlayerBannerManager;
    private void Start()
    {
        // PhotonNetwork.ConnectUsingSettings();
        //need to get the past users as well when we join the room... maybe make an override method that takes in a list of players
        Player[] players = PhotonNetwork.PlayerList;

        foreach (var player in players)
        {
            Debug.Log("Player actor number:" + player.ActorNumber.ToString());

        }



        PlayerBannerManager = FindObjectOfType<PlayerBannerManager>();
        SpawnManager = GetComponent<MatchSpawnManager>();
    }

    #region Room Events
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        //for now this will auto connect...
        PhotonNetwork.JoinLobby(); //this can be done when we press "Online game" perhaps...

    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        //we can create the room from here...

        //here we will show the player connect UI
        ConnectionPanel.SetActive(true);

    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

    }

    public void OnJoinedMatchRoom()
    {


        //here we will go into a lobby, and the master client can start the match when enough people have joined...
        ConnectionPanel.SetActive(false);




        //this will stay in this class
        SetupLobby(); // this will be called on, on scenemanage load

    }

    private void SetupLobby()
    {
        LobbyFakePanel.SetActive(true);
        UpdatePlayerNames();

        if (PlayerBannerManager == null)
            PlayerBannerManager = FindObjectOfType<PlayerBannerManager>();


        //need to get the past users as well when we join the room... maybe make an override method that takes in a list of players
        Player[] players = PhotonNetwork.PlayerList;

        foreach (var player in players)
        {
            Debug.Log("Player actor number:" + player.ActorNumber.ToString());
            PlayerBannerManager.SpawnPlayerBadge(player);
        }


        Debug.Log("We reach here");


        if (PhotonNetwork.IsMasterClient)
        {
            StartButton.gameObject.SetActive(true);

            StartButton.interactable = false;

            if (PhotonNetwork.CurrentRoom.PlayerCount >= RequiredPlayers)
            {
                StartButton.interactable = true;

            }
        }
    }

    public void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 4;
        roomOptions.CleanupCacheOnLeave = true;
        PhotonNetwork.JoinOrCreateRoom(RoomCode.text, roomOptions, TypedLobby.Default);

        PhotonNetwork.NickName = PlayerName.text;
    }


    #endregion

    #region Player Room Events
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);

        if(PhotonNetwork.LocalPlayer != newPlayer)
            PlayerBannerManager.SpawnPlayerBadge(newPlayer);

        //this is for the master client...

        if (PhotonNetwork.IsMasterClient)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount >= RequiredPlayers)
            {
                StartButton.interactable = true;

            }
        }

        UpdatePlayerNames();

    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);

        //need to check if the number of players left is under the play threshold...
        //then if the match has already started, we go straight to the end match..
        PlayerBannerManager.RemovePlayerBadge(otherPlayer);


        if (PhotonNetwork.IsMasterClient)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount <= RequiredPlayers - 1)
            {
                if (startTimer)
                    EndMatch();
                StartButton.gameObject.SetActive(true);
                StartButton.interactable = false; //so they can't do a solo game...
            }
        }

        UpdatePlayerNames();

    }
    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);

        previousStart = startTimer;

        object propsTime;
        if (propertiesThatChanged.TryGetValue("StartTime", out propsTime))
        {
            startTime = (double)propsTime;
        }

        object propsBool;
        if (propertiesThatChanged.TryGetValue("Started", out propsBool))
        {
            if (previousStart != (bool)propsBool)
            {
                startTimer = (bool)propsBool;

                Debug.Log("Match status: " + startTimer.ToString());



                if (startTimer)
                    StartMatch();
                else
                    EndMatch(); //this should be called.. but it isn't... annoying
            }
        }

        

        if (propertiesThatChanged.TryGetValue("ShowLobby", out propsBool)) // change this to the new system of enums and store that in the room props
        {
            if (LobbyFakePanel.activeInHierarchy != (bool)propsBool)
            {
                bool val = (bool)propsBool;


                Debug.Log("We are toggling the lobby: " + val.ToString());

                LobbyFakePanel.SetActive(val);
                if ((bool)propsBool)
                    WinPanel.SetActive(false);

            }
        } //plz work


    }
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);


        if (targetPlayer == PhotonNetwork.LocalPlayer)
        {
            object prop;
            if (changedProps.TryGetValue("gold", out prop))
            {
                SetGoldUI((int)prop);
            }
        }


        //update the player badges here... so it will get rid of the set gold ui part, as it will be handled by the PlayerBannerManager.cs
        PlayerBannerManager.UpdatePlayerBadge(targetPlayer);
    }
    #endregion

    #region Game Timer;



    bool startTimer = false; //might have to move this to a flag..
    private bool previousStart;
    double timerIncrementValue;
    double startTime;
    [SerializeField] double timer = 20;
    ExitGames.Client.Photon.Hashtable CustomeValue;

    public void StartMatch()
    {
        //also need to start the match timer here I believe, however need to figure out how to do a match countdown just before it starts...
        LobbyFakePanel.SetActive(false);
        Debug.Log("We have started a match");
        GamePanel.SetActive(true);

        GameObject go = PhotonNetwork.Instantiate(PlayerPrefab.name, SpawnPoints[Random.Range(0, SpawnPoints.Length)].position, Quaternion.identity);
        PlayerController player = go.GetComponent<PlayerController>();

        player.ToggleComponents(); // this turns on all the components so we don't have overwritting commands

        ClearPlayerNames();

        SetGoldUI(0);

        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            CustomeValue = new ExitGames.Client.Photon.Hashtable();
            startTime = PhotonNetwork.Time;
            startTimer = true;
            CustomeValue.Add("ShowLobby", false);
            CustomeValue.Add("StartTime", startTime);
            CustomeValue.Add("Started", startTimer);
            PhotonNetwork.CurrentRoom.SetCustomProperties(CustomeValue);


        }

        SpawnManager.SpawnStart();


    }

    void EndMatch(bool naturalEnd = false)
    {
        //need to return to the lobby..

        //so need to destory the playerprefabs... //need to account for the player leaving...

        PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.LocalPlayer);

        //reenable the lobby screen...
        WinPanel.SetActive(true);
        GamePanel.SetActive(false);


        //also reset player scores... as score will be used to as gold sub
        SpawnManager.SpawnEnd();



        if (PhotonNetwork.IsMasterClient)
        {




            CustomeValue = new ExitGames.Client.Photon.Hashtable();

            startTimer = false;

            CustomeValue.Add("Started", startTimer);
            PhotonNetwork.CurrentRoom.SetCustomProperties(CustomeValue);


        }


        OnMatchNaturalEnd();

        //so if the game ends naturally..

        //we need to show a winning screen

        //and then we can just have a button that will go back to the normal lobby...
    }

    void Update()
    {


        if (!startTimer) return; //iif the game hasn't started yet, don't do shit

        //place the game mode into here


        timerIncrementValue = startTime - PhotonNetwork.Time;


        if (GameTimeText != null)
            GameTimeText.text = ((int)timer + (int)timerIncrementValue).ToString(); // this needs to be moved out of the update function REDO


        //if (PhotonNetwork.IsMasterClient)
        //{




        //}



        if (((int)timer + (int)timerIncrementValue) <= 0)
        {
            //Timer Completed
            //Do What Ever You What to Do Here
            startTimer = false;
            Debug.Log("Timer has finised... match should end now...");


            EndMatch(true);
        }



    }

    #endregion

    void OnMatchNaturalEnd()
    {
        Player[] players = PhotonNetwork.PlayerList;

        Player highestPlayer = null;
        int highest = -1;
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].GetGold() > highest)
            {
                highest = players[i].GetGold();
                highestPlayer = players[i];
            }

        }

        if (highestPlayer != null)
        {
            WinnerName.text = highestPlayer.NickName;
            WinAmount.text = highestPlayer.GetGold().ToString();
        }

        if (PhotonNetwork.IsMasterClient)
        {
            StartButton.gameObject.SetActive(true);
            PlayAgainButton.interactable = true;
        }

        //so we have the winner... // this should be secured by the master client... this will be for future polish

        //toggle the win screen... this might be a in-game thing...


        //timer then a button will appear

        //can use linq to sort this list... by highest score?


        if (PhotonNetwork.CurrentRoom.PlayerCount >= RequiredPlayers)
        {
            StartButton.interactable = true;

        }

        for (int i = 0; i < players.Length; i++)
        {
            players[i].SetGold(0); //reset the players gold amounts..
        }

        UpdatePlayerNames();

    }


    public void PlayAgain()
    {
        LobbyFakePanel.SetActive(true);
        WinPanel.SetActive(false);

        StartButton.interactable = false;
        PlayAgainButton.interactable = false;



        if (PhotonNetwork.CurrentRoom.PlayerCount >= RequiredPlayers)
        {
            StartButton.interactable = true; //this is just a double check to make sure we have enough players for another match
        }



        CustomeValue = new ExitGames.Client.Photon.Hashtable();
        CustomeValue.Add("ShowLobby", true);
        PhotonNetwork.CurrentRoom.SetCustomProperties(CustomeValue);


        //we need to move all the player clients back to teh fake lobby screen
        //send an rpc to renable the lobby screen...
    }


    #region UI
    public void SetHealthUI(int amount)
    {
        //this will be changed to an icon based system...
        PlayerHealthText.text = "Health: " + amount.ToString();



    }


    public void SetGoldUI(int amount)
    {
        GoldText.text = "Gold: " + amount.ToString();
    }

    public void SetPlayerLobbyName(string name)
    {
        for (int i = 0; i < PlayerNames.Length; i++)
        {
            if (string.IsNullOrEmpty(PlayerNames[i].text))
            {
                //here we need to highlight the player who is the host

                PlayerNames[i].text = name;
                break;
            }
        }
    }

    public void ClearPlayerNames()
    {

        for (int i = 0; i < PlayerNames.Length; i++)
        {
            PlayerNames[i].text = "";
        }
    }


    public void UpdatePlayerNames()
    {
        ClearPlayerNames();
        Player[] players = PhotonNetwork.PlayerList;
        for (int i = 0; i < players.Length; i++)
        {
            SetPlayerLobbyName(players[i].NickName);
        }

    }
    #endregion
}
