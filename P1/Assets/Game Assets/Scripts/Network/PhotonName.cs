﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotonName : MonoBehaviourPun
{
    public Text PlayerNameText;
    private void Start()
    {
        if(PlayerNameText != null && photonView.Owner != null)
            PlayerNameText.text = photonView.Owner.NickName;
    }

    private void OnEnable()
    {
        if (PlayerNameText != null && photonView.Owner != null)
            PlayerNameText.text = photonView.Owner.NickName;
    }
}
