﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PhotonSpriteView : MonoBehaviour, IPunObservable
{
    public bool SyncFlips;

    private SpriteRenderer _sr;

    private void Start()
    {
        _sr = transform.Find("Visual").GetComponentInChildren<SpriteRenderer>();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            if (SyncFlips && _sr != null)
            {
                stream.SendNext(_sr.flipX); //might move this into it's own class "Photon Sprite 2D" <- sounds good
                stream.SendNext(_sr.flipY); //might move this into it's own class "Photon Sprite 2D" <- sounds good
            }
        }
        else
        {
            if (SyncFlips && _sr != null)
            {
                _sr.flipX = (bool)stream.ReceiveNext();
                _sr.flipY = (bool)stream.ReceiveNext();
            }
        }
    }
}
