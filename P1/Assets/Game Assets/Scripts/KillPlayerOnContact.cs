﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayerOnContact : MonoBehaviour
{
    public int DamageAmount;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Ignore"))
        {
            IHealth target = collision.GetComponent<IHealth>();

            if (target != null && !target.IsDead())
            {
                collision.GetComponent<PlayerHealth>().TakeDamage(DamageAmount);

            }
        }
    }
}
