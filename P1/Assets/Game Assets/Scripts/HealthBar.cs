﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    //this is a visual component
    public int Current;

    public Color Color;
    public Image[] Hearts;

    public void UpdateHearts(int amount)
    {
        Current = amount;
        Debug.Log(amount);

        //this system needs to be reworked, as it won't go down to 1 heart, either 0, 2, 3

        for (int i = 0; i < Hearts.Length; i++)
        {

            Hearts[i].enabled = false;

            if (i < Current && Current != 0)
                Hearts[i].enabled = true;
        }



    }
    
}
