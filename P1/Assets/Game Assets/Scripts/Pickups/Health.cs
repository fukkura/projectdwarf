﻿using ExitGames.Client.Photon.StructWrapping;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviourPun, IPickup
{
    public int Amount;

    public float DestroyTime;


    void OnEnable()
    {
        if (photonView.IsMine)
            StartCoroutine(Destroy());
    }


    public void Pickup(Player player, GameObject gameObject = null)
    {

        if (gameObject == null || !player.IsLocal)
            return;


        gameObject.GetComponent<IHealth>().GiveHealth(Amount);


        GetComponent<PhotonView>().RPC("ToggleViewNetwork", RpcTarget.All, false);

        //PhotonNetwork.Destroy(this.gameObject); //might move this to an RPC call

    }
    [PunRPC]
    public void ToggleViewNetwork(bool value = false)
    {
        gameObject.SetActive(value);
    }

    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(DestroyTime);

        PhotonNetwork.Destroy(gameObject);

    }
}
