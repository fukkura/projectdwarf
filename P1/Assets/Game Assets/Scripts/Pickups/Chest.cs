﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviourPun, IInteractable
{
    public GameObject Gold;
    public Transform SpawnPoint;
    //when the chest is opened, it might take a second or two, just to give a reason for the player to stay still, giving other players a chance to kill this player..
    public int DestoryTime = 5;
    public ParticleSystem ps;
    public ParticleSystem spawnps;
    private bool _opened;

    private Animator _anim;

    private void Start()
    {
        _anim = GetComponent<Animator>();
    }
    private void OnEnable()
    {
        StartCoroutine(Destory());
        _opened = false;
        spawnps.Play();
    }
    public void Interact(Player player)
    {
        Debug.Log("This chest was opened");

        if (_opened)
            return;
        //play an open animation
        _anim.SetTrigger("Open");

        //spawn a random amount of coins...
        //this will use a basic pickup class...
        //FindObjectOfType<MatchSpawnManager>().SpawnGold(transform.position); removed as no longer going to use gold coins
        player.AddGold(Random.Range(3, 7));
        //have a animation event on the animation, to delete the chest from the match...
        _opened = true;

        ps.Play();
        StartCoroutine(Destory());
    }

    private IEnumerator Destory()
    {
        yield return new WaitForSeconds(DestoryTime);

        _anim.SetTrigger("Close");


        if (photonView.IsMine)
            PhotonNetwork.Destroy(gameObject);

    }
}
