﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    #region Singleton
    public static GameManager Instance;
    private void Awake()
    {
        if (GameManager.Instance != null)
            Destroy(gameObject);
        else
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
    #endregion

    public bool UsePresence;
    private long _startTime;
    private void Start()
    {
        if (!UsePresence)
            return;

        _startTime = DiscordPresence.DiscordTime.TimeNow();


        DiscordPresence.PresenceManager.UpdatePresence("Hanging around in", "Menus", _startTime); //set the presences title for now...
    }








}
