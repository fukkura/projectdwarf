﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSettings : MonoBehaviour
{
    public Animator _anim;
    private SpriteRenderer _sr;
    private PlayerMovement _move;
    private PlayerHealth _health;
    private void Start()
    {
        _sr = transform.Find("Visual").GetComponentInChildren<SpriteRenderer>();
        _move = GetComponent<PlayerMovement>();
        _health = GetComponent<PlayerHealth>();
    }


    public void Flip(int side)
    {
        if (_move.wallGrab || _move.wallSlide)
        {
            if (side == -1 && _sr.flipX)
                return;

            if (side == 1 && !_sr.flipX)
            {
                return;
            }
        }

        bool state = (side == 1) ? false : true;
        _sr.flipX = state;
    }

    public int GetSide()
    {
        return _sr.flipX ? -1 : 1;
    }

    public void SetAnimationsParameters(bool ismoving = false, bool isgrounded = false, bool iswall = false)
    {
        if (_anim == null)
            return;


        _anim.SetBool("isMoving", ismoving);
        _anim.SetBool("isGrounded", isgrounded);
        _anim.SetBool("isWall", iswall);

        if (_health != null)
            _anim.SetBool("isDead", _health.IsDead());
    }
}
