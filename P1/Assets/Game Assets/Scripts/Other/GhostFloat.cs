﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class GhostFloat : MonoBehaviourPun
{
    public float MovementSpeed;
    private void OnEnable()
    {
        //move the object upwards, then redisable

        StartCoroutine(Destroy());
    }

    private void Update()
    {
        if (photonView.IsMine)
            transform.position += Vector3.up * MovementSpeed * Time.deltaTime;




    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(5f);
        if (photonView.IsMine)
            PhotonNetwork.Destroy(gameObject);
    }
}
