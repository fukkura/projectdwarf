﻿using DG.Tweening;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicProjectile : MonoBehaviourPun
{
    private Rigidbody2D _rb;

    public int Damage = 10;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();


        //

    }

    public void Init(Vector2 dir, float speed = 4)
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.velocity = dir * speed; // I think this will work..
        _rb.AddTorque(10f, ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("We hit an object: " + collision.gameObject.name);

            //place rpc here to check who we hit... and then send damage

        if(collision.CompareTag("Player"))
        {

            IHealth target = collision.GetComponent<IHealth>();

            if (target != null)
            {
                if (!PhotonNetwork.OfflineMode)
                {
                    PhotonView colPv = collision.GetComponent<PhotonView>();


                    colPv.RPC("TakeDamage", RpcTarget.Others, Damage);


                }
                else
                    collision.GetComponent<PlayerHealth>().TakeDamage(Damage);
            }

        }

        //here we need to spawn a particle system and have it play, then destroy itself
        PhotonNetwork.Instantiate("HitParticle", transform.position, Quaternion.identity);

        if(photonView.IsMine)
            PhotonNetwork.Destroy(gameObject); //I don't like how these are being destroyed... so will move to an object pool base
    }

}
