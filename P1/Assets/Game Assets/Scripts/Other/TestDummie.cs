﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDummie : MonoBehaviour, IHealth
{
    public void GiveHealth(int amount)
    {
        throw new System.NotImplementedException();
    }

    public bool IsDead()
    {
        throw new System.NotImplementedException();
    }

    public void TakeDamage(int amount)
    {
        Debug.Log("I was hit for: " + amount.ToString());
    }

   
}
