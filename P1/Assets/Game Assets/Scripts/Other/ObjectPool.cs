﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pool
{
    public string ID;
    public GameObject Prefab;
    public int Amount;
}
public class ObjectPool : MonoBehaviour
{
    public List<Pool> Pools;
    public Dictionary<string, Queue<GameObject>> PoolDatabase;

    public Transform Parent;
    private void Start()
    {
        PoolDatabase = new Dictionary<string, Queue<GameObject>>();

        foreach (var pool in Pools)
        {
            Queue<GameObject> gameobjects = new Queue<GameObject>();

            for (int i = 0; i < pool.Amount; i++)
            {
                GameObject go = Instantiate(pool.Prefab, Parent);
                go.SetActive(false);
                gameobjects.Enqueue(go);
            }

            PoolDatabase.Add(pool.ID, gameobjects);
        }    


    }

    public GameObject Spawn(string id)
    {
        GameObject go = PoolDatabase[id].Dequeue();

        PoolDatabase[id].Enqueue(go);

        return go;
    }

    public void Despawn(GameObject go)
    {
        go.SetActive(false);
    }



}
