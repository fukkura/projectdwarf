﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBannerManager : MonoBehaviourPun
{
    public GameObject PlayerBadgePrefab;
    public Transform Parent;
    //we could share the players health via the sethealth function in punscore extension...

    //as we can use the OnPlayerPropsupdate function to update the persons playerbadge, as gold is already linked on there...
    private List<GameObject> _playerBadges;

    public void SpawnPlayerBadge(Player player) //so how are we going to share the players health...... and make sure the badges are in order...    
    {

        if (_playerBadges == null)
            _playerBadges = new List<GameObject>();
        //also need to do a check to see if the object is already spawned

        //I don't think the order of the players is changed
        GameObject go = Instantiate(PlayerBadgePrefab, Parent);


        go.transform.name = player.ActorNumber.ToString();
        Text playerName = go.transform.Find("PlayerName").GetComponent<Text>();
        Text playerGold = go.transform.Find("PlayerGold").GetComponent<Text>();

        playerName.text = player.NickName;
        playerGold.text = "Gold: " + player.GetGold();
        UpdateHealthBar(go, player);

        _playerBadges.Add(go);

    }

    public void RemovePlayerBadge(Player player)
    {
        GameObject toRemove = null;
        foreach (var go in _playerBadges)
        {
           if( go.name == player.ActorNumber.ToString())
            {
                //need to remove it
                toRemove = go;
            }
        }

        if (toRemove != null)
        {
            _playerBadges.Remove(toRemove);
            Destroy(toRemove);
        }

    }
   
    public void UpdatePlayerBadge(Player player)
    {
        //HERE WE WILL UPDATE THE PLAYERS PANNEL...

        foreach (var go in _playerBadges)
        {
            if (go.name == player.ActorNumber.ToString())
            {
                Text playerGold = go.transform.Find("PlayerGold").GetComponent<Text>();

                playerGold.text = "Gold: " + player.GetGold();


                UpdateHealthBar(go, player);
            }
        }


    }

    //might need calls to the playerjoin and left so we can remove the panels... or add one

    public void UpdateHealthBar(GameObject go, Player player)
    {

        //update player health here...
        HealthBar bar = go.transform.Find("Panel").GetComponent<HealthBar>();


        if (bar != null)
        {
            bar.UpdateHearts(player.GetHealth());
        }




    }
}
