﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //publics
    [Range(1, 10)]
    public float moveSpeed;
    public float jumpForce;
    public float slideSpeed;
    public float wallJumpLerp = 10;

    [Space]

    public bool wallGrab;
    public bool wallJump;
    public bool canMove;
    public bool wallSlide;

    [Header("PS")]
    public ParticleSystem JumpPS;
    public ParticleSystem JumpWallPS;
    public ParticleSystem SlidePS;

    //privates
    private Rigidbody2D _rb;
    private Collision _coll;

    private AnimationSettings _anim;

    private int _facingSide = 1;
    private int _jumpCount = 0;
    private bool _touchedGround;
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _coll = GetComponent<Collision>();
        _anim = GetComponent<AnimationSettings>();
    }

    private void OnEnable()
    {
        _jumpCount = 0;
        canMove = true;
    }
    public void SetFacingDir(int side)
    {
        _facingSide = side;
    }

    public int GetFacingDir()
    {
        return _facingSide;
    }

    private void Update() //this whole method needs to be cleaned up;
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        Vector2 dir = new Vector2(x, y);

        wallGrab = _coll.onWall && Input.GetKey(KeyCode.LeftShift);

        if (wallGrab)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, y * moveSpeed);
        }

        if (_coll.onWall && !_coll.onGround && !wallGrab)
        {

            WallSlide();
        }

        if (_coll.onWall)
        {
            int side = _coll.onRightWall ? -1 : 1;
            _anim.Flip(side);
        }
        else
        {

            if (x > 0)
            {
                _facingSide = 1;
                _anim.Flip(_facingSide);
            }
            if (x < 0)
            {
                _facingSide = -1;
                _anim.Flip(_facingSide);
            }

        }

        if (Input.GetButtonDown("Jump"))
        {

            if (_jumpCount <= 1 && !_coll.onWall)
                Jump(Vector2.up);
            else if (_coll.onWall && !_coll.onGround)
            {
                WallJump();
            }
        }


        if (_coll.onGround && !_touchedGround)
        {
            GroundTouch();
            _touchedGround = true;
        }

        if (!_coll.onGround && _touchedGround)
        {
            _touchedGround = false;
        }

        Walk(dir);

        WallParticle(y);

        _anim.SetAnimationsParameters(_rb.velocity != Vector2.zero, _coll.onGround, _coll.onWall);
    }

    private void GroundTouch()
    {
        _jumpCount = 0;
        JumpPS.Play();
    }

    private void Walk(Vector2 dir) //used for moving the player when grounded
    {

        if (wallGrab || !canMove)
            return;



        if (!wallJump)
            _rb.velocity = (new Vector2(dir.x * moveSpeed, _rb.velocity.y));
        else
            _rb.velocity = Vector2.Lerp(_rb.velocity, (new Vector2(dir.x * moveSpeed, _rb.velocity.y)), wallJumpLerp * Time.deltaTime);
    }

    private void WallSlide()
    {

        _rb.velocity = new Vector2(_rb.velocity.x, -slideSpeed);
    }

    private void Jump(Vector2 dir, bool wall = false) //WYSIWYG
    {
        _rb.velocity = new Vector2(_rb.velocity.x, 0);
        _rb.velocity += dir * jumpForce;
        _jumpCount += 1;
        JumpPS.Play();
    }

    private void WallJump()
    {
        if ((_facingSide == 1 && _coll.onRightWall) || _facingSide == -1 && !_coll.onRightWall)
        {
            _facingSide *= -1;
            _anim.Flip(_facingSide);
        }

        StopCoroutine(DisableMovement(0));
        StartCoroutine(DisableMovement(.1f));

        Vector2 wallDir = _coll.onRightWall ? Vector2.left : Vector2.right;

        Jump((Vector2.up / 1.5f + wallDir / 1.5f), true);

        wallJump = true;
    }

    void WallParticle(float vertical)
    {
        var main = SlidePS.main;

        if (wallSlide || (wallGrab && vertical < 0))
        {
            SlidePS.transform.parent.localScale = new Vector3(ParticleSide(), 1, 1);
            main.startColor = Color.white;
        }
        else
        {
            main.startColor = Color.clear;
        }
    }

    int ParticleSide()
    {
        int particleSide = _coll.onRightWall ? 1 : -1;
        return particleSide;
    }

    //Ienumtors
    IEnumerator DisableMovement(float time)
    {
        canMove = false;
        yield return new WaitForSeconds(time);
        canMove = true;
    }
}
