﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class PlayerController : MonoBehaviourPun
{

    //this will be used to managed the network side of things, 
    //it will check if the photonView is local, if it is, it will enable all the componets needed for the player..
    [Header("Components")]

    //I can change this to a list of monobehavour sub classes, that just have to be enabled...
    //allows for more modulatory
    public List<MonoBehaviour> monos;
    public Rigidbody2D rb;
    public SpriteRenderer sr;
    public string localLayer;
    public string defaultLayer;
    private void Start()
    {
        //transform.name = photonView.OwnerActorNr.ToString();
    }
    public void ToggleComponents(bool value = true)
    {
        if(rb != null)
            rb.isKinematic = false;

        if(value)
        {
            photonView.TransferOwnership(PhotonNetwork.LocalPlayer);

            photonView.Owner.ClearGold();
            sr.enabled = true;
        }



        int layer = value ? LayerMask.NameToLayer(localLayer) : LayerMask.NameToLayer(defaultLayer);
        gameObject.layer = layer;
        gameObject.tag = value ? "Ignore" : "Player";

        foreach (var mono in monos)
        {
            mono.enabled = value;
        }
    }

    private void OnDisable()
    {
        ToggleComponents(false);
        sr.enabled = false;

    }

}
