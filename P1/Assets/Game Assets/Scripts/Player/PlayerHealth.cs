﻿using DG.Tweening;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class PlayerHealth : MonoBehaviourPun, IHealth
{
    public int currentHealth;
    public int maxHealth;

    public GameObject Visuals;

    private bool _isDead;
    private MatchManager _matchManager;
    private void Start()
    {
        currentHealth = maxHealth;

        _matchManager = FindObjectOfType<MatchManager>();
        _matchManager.SetHealthUI(currentHealth);

    }

    private void OnEnable()
    {
        currentHealth = maxHealth;

        if (_matchManager == null)
            _matchManager = FindObjectOfType<MatchManager>();


        _isDead = false;
        _matchManager.SetHealthUI(currentHealth);

        photonView.Owner.SetHealth(currentHealth);
    }

    [PunRPC]
    public void GiveHealth(int amount)
    {


        if (!photonView.IsMine)
            return;

        int tempHealth = currentHealth + amount;

        tempHealth = Mathf.Clamp(tempHealth, 0, maxHealth);

        currentHealth = tempHealth;

        _matchManager.SetHealthUI(currentHealth);
        photonView.Owner.SetHealth(currentHealth);

    }

    [PunRPC]
    public void TakeDamage(int amount)
    {
        if (!photonView.IsMine || _isDead)
            return;

        int tempHealth = currentHealth - amount;

        if (tempHealth <= 0)
        {
            currentHealth = 0;

            //we should die here
            Die();
            _matchManager.SetHealthUI(currentHealth);
            photonView.Owner.SetHealth(currentHealth);

            //need to lock the camera when shaking
            Camera.main.transform.DOShakePosition(0.5f, 0.5f);
            return;
        }


        currentHealth = tempHealth;
        _matchManager.SetHealthUI(currentHealth);

        Camera.main.transform.DOShakePosition(0.25f, 0.25f); //just testing
        photonView.Owner.SetHealth(currentHealth);

    }

    private void Die()
    {
        Debug.Log("I am dead: " + transform.name);

        //play a death aninmation

        //need to make a call to the MatchSpawner, to take some of your gold, a percentage of the total amount, and drop it on the floor
        //so will need to disable the collider on the player, so he doesn't pick it up
        FindObjectOfType<MatchSpawnManager>().SpawnGhost(transform.position);
        StartCoroutine(Respawn(2));
        //begin timer to respawn...
    }

    private IEnumerator Respawn(int time)
    {


        //prevent movement..
        //disable hitboxes.. slight problem here.. we will need to sync the hitboxes across the network
        //so other palyers can't just hit the dead body oevr and over

        //possible soluation would be to hide the player, but spawn a death sprite...

        _isDead = true;

        //Visuals.SetActive(false);

        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        PlayerMovement movement = GetComponent<PlayerMovement>();

        movement.canMove = false;

        if (rb != null)
            rb.isKinematic = true;



        //need to spawn some player coins here...

        int goldAmount = photonView.Owner.GetGold();
        goldAmount = goldAmount / 2;

        //if (goldAmount != 0)
        //FindObjectOfType<MatchSpawnManager>().SpawnGold(transform.position + (Vector3.up * 2), goldAmount);

        photonView.Owner.SetGold(goldAmount);
        _matchManager.SetGoldUI(goldAmount);

        yield return new WaitForSeconds(time);

        movement.canMove = true;

        //place at point bug here.. need to fix 
        transform.position = _matchManager.SpawnPoints[Random.Range(0, _matchManager.SpawnPoints.Length)].position;

        _isDead = false;
        if (rb != null)
            rb.isKinematic = false;

        //reset the animation..

        currentHealth = maxHealth;

        _matchManager.SetHealthUI(currentHealth);
        photonView.Owner.SetHealth(currentHealth);

        //Visuals.SetActive(true);

    }

    public bool IsDead()
    {
        return _isDead;
    }
}
