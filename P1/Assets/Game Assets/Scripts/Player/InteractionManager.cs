﻿using ExitGames.Client.Photon.StructWrapping;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionManager : MonoBehaviourPun
{
    PlayerHealth PlayerHealth;


    private IconManager _icon;
    private void Start()
    {
        PlayerHealth = GetComponent<PlayerHealth>();

        _icon = GetComponentInChildren<IconManager>();

        _icon.UpdateIcon(IconType.NORMAL);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!photonView.IsMine)
            return;

        if (PlayerHealth.IsDead())
            return;

        if (collision.CompareTag("Pickup"))
        {
            PhotonView pv = collision.gameObject.GetComponent<PhotonView>();

            if (pv.Owner != photonView.Owner) //so we aren't overloading with transfer requests
                pv.TransferOwnership(photonView.Owner);


            collision.GetComponent<IPickup>().Pickup(photonView.Owner, gameObject); //this is for stuff like gold
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {

        if (!photonView.IsMine)
            return;

        if (PlayerHealth.IsDead())
            return;

        if (collision.CompareTag("Interact"))
        {
            PhotonView pv = collision.gameObject.GetComponent<PhotonView>();

            if (pv.Owner != photonView.Owner) //so we aren't overloading with transfer requests
                pv.TransferOwnership(photonView.Owner);

            _icon.UpdateIcon(IconType.INTERACT);
            //need to show a interaction button

            if (Timer())
            {

                _icon.UpdateIcon(IconType.NORMAL);

                collision.GetComponent<IInteractable>().Interact(photonView.Owner); //like chests
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        _icon.UpdateIcon(IconType.NORMAL);

    }




    private float _currentTime;
    private int _debugTime = 1;
    public bool Timer() //this method actually seems to work.. I don't know why...
    {
        if (Input.GetKey(KeyCode.E))
        {
            _currentTime += Time.deltaTime;


            if (_currentTime >= _debugTime)
            {
                //we can open the chest
                _currentTime = 0;// to reset the counter for another use

                return true;
            }
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            _currentTime = 0;
        }

        return false;
    }
}
