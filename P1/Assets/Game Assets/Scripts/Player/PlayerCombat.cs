﻿using ExitGames.Client.Photon.StructWrapping;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{

    public Transform overlapPoint;
    public float overlapRadius;

    public LayerMask targetLayer;


    //we will have scriptable objects for different weapons eventually

    public int damageAmount;

    //instance of projectile..
    public GameObject projectile;


    public bool useProjectiles;

    //Mixture method of combat, if an enemy player is close it does more damage than the ranged based attack, but these attacks happen at the same time
    //ranged and close melee

    private MatchSpawnManager spawnManager;
    private AnimationSettings _anim;

    //First
    /*
     * Press attack
     *  Play animation, with will have an event to enable a close collider to check for collisions, or we do a physics2D overlap check
     *  Also fire a projectile  towards the mouse? or just in a side direction... will try both 
     *  
     *  
     *  if the projectile hits the player or the melee attack does.. deal damage...
     */

    private void Start()
    {
        spawnManager = FindObjectOfType<MatchSpawnManager>();
        _anim = GetComponent<AnimationSettings>();
    }


    private void Update()
    {
        //temp button press to call the attack funtion
        if(Input.GetMouseButtonDown(0)) //need to be changed when ART is put into the game
        {
            Attack();
        }
    }


    public void Attack() //this is called by the animation....
    {
        //we will use a physics overlap
       int side = _anim.GetSide();

        //so what side we are face, we need to do a physics overlap there...
        Vector3 pos = transform.position + (Vector3.right * side);

       Collider2D[] colliders = Physics2D.OverlapCircleAll(pos, overlapRadius, targetLayer);

        foreach (var col in colliders)
        {
           IHealth target = col.GetComponent<IHealth>();
            if (target != null)
            {
                if (!PhotonNetwork.OfflineMode && PhotonNetwork.IsConnected)
                    col.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.Others, damageAmount);
                else
                    target.TakeDamage(damageAmount);
            }
            ////this will have to be moved to a RPC eventually
        }

        if (!useProjectiles)
            return;
         // there will be a check here if the melee hit anything... so not to spam spawn
        //then spawn a projectile...

        //so we need to get the direction from the spawn point, to the mouse point on screen,
        //this will be used to give adirection to the projectile...
        Vector3 dir = pos - transform.position;
        dir = dir.normalized;

        if(spawnManager != null)
            spawnManager.SpawnProjectile(pos, dir, 16f); 


    }


    private void OnDrawGizmos()
    {
        if (_anim == null)
            return;

        Gizmos.color = Color.green;

        //we will use a physics overlap
        int side = _anim.GetSide();

        //so what side we are face, we need to do a physics overlap there...
        Vector3 pos = transform.position + (Vector3.right * side);
        Gizmos.DrawWireSphere(pos, overlapRadius);
    }
}
