﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviourPun, IPickup
{
    public int Amount;
    void IPickup.Pickup(Player player, GameObject gameObject = null)
    {
        //so we need to get the player who picked this up..

        //add the amount to the score...
        player.AddGold(Amount);
        //then delete teh object
        PhotonNetwork.Destroy(gameObject);
    }
}
