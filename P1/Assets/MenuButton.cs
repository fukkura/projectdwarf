﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    public GameObject MenuObject;

    public bool Enable;
    public void ToggleMenu()
    {
        if (MenuObject != null)
            MenuObject.SetActive(Enable);
    }
}
