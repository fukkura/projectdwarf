﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviourPun
{
    // Start is called before the first frame update
    private ParticleSystem _ps;
    private void Awake()
    {
        _ps = GetComponent<ParticleSystem>();

    }

    private void Update()
    {
        if (_ps.isStopped)
        {
            PhotonNetwork.Destroy(photonView);
        }
    }
    private void OnEnable()
    {

        if (_ps == null) // this needs to be redone, but it works as is
            _ps = GetComponent<ParticleSystem>();

        if(_ps != null)
            _ps.Play();
    }

    private void OnDisable()
    {
        if (_ps != null)
            _ps.Stop();
    }
}
