﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum IconType
{
    NORMAL,
    INTERACT

}
public class IconManager : MonoBehaviour
{
    public Sprite NormalIcon;
    public Sprite InteractIcon;


    private SpriteRenderer _sr;
    private IconType _type;
    private void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
    }
    public void UpdateIcon(IconType type) //just used to update the sprite that should be shown when 
    {
        if (_type != type)
        {
            switch (type)
            {
                case IconType.NORMAL:
                    _sr.sprite = NormalIcon;
                    break;
                case IconType.INTERACT:
                    _sr.sprite = InteractIcon;

                    break;
                default:
                    _sr.sprite = NormalIcon;

                    break;
            }
        }

        _type = type;
    }
}
